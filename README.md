# ReactExample

This is an example of react js + node js + mongodb.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

First, you need to install node. You can download from [here](https://nodejs.org/en/).
Then you need to download and install [MongoDB](https://www.mongodb.com/).

### Installing

Install dependencies (in the app root directory and in the /client directory)
```
npm install
```
Start MongoDB (assume that you have installed MongoDB to C:\Program  Files
```
cd C:\Program Files\MongoDB\Server\4.2\bin
./mongod
```
Connect to db using MongoDB community server(v 4.2). You can download from [here](https://www.mongodb.com/download-center/community)
And then create a database named "alpha", then import collections by using json files in db folder.

The DB structure is like this:
  db name : alpha
  collections:
  	- basicinfos
  	- messages
  	- mixes
  	- news
  	- portfolios
  	- products
  	- quotes
  	- sectors
  	- services
  	- settings
  	- slides
  	- teams
  	- users

Run the app (in the app root directory)(this is developer mode)
```
npm run dev
```
You can see the site on localhost:3000

After this, you can use production mode(localhost:5000)
```
npm start
```
OK?

